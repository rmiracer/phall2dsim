phall_zphi
{
  Axial-azimuthal model of Hall thruster.

  Author: Rodrigo Andres Miranda Cerda

  2019/05/10

   Changed parameters following Coche and Garrigues, Physics of Plasmas 21, 023503, 2014.

  2019/02/12

    Model created following Adam et al., Physics of Plasmas 11 (1), 2004.
}
Variables
{
   JMAX = 256
   KMAX = 256
   maxLength = 0.04
   maxHeight = 0.02

   modBmax = 170*1.e-4 // nT
   sigmaBFieldsquared1 = 0.0080*0.0080
   sigmaBFieldsquared2 = 0.0200*0.0200
   muxBField = 0.025

   plasmaDensityMKS = 1e16
   ne_max = 3e19

   // Ion mass
   PROTONMASS = 1.672e-27

   IONMASS = 131.293*PROTONMASS

   DT = 1.e-11
   NPIM = 1e7
}
Region
{
  Grid
  {
    J = JMAX
    x1s = 0.0
    x1f = maxLength
    n1 = 1.0
    K = KMAX
    x2s = 0.0
    x2f = maxHeight
    n2 = 1.0

    Geometry = 1
    PeriodicFlagX1 = 0
    PeriodicFlagX2 = 1
  }
  Control
  {
    dt = DT
    ElectrostaticFlag = 4

    B01analytic = 0.
    B02analytic = 0.
    B03analytic = step(muxBField - x1)*modBmax*exp( -(x1 - muxBField)*(x1 - muxBField)/sigmaBFieldsquared1 ) + step(x1 - muxBField )*modBmax*exp( -(x1 - muxBField)*(x1 - muxBField)/sigmaBFieldsquared2 )
  }
  MCC
  {
    gas = Xe
    eSpecies = electrons
    iSpecies = ions
    analyticF = ne_max*((1 - (exp(2*(x1-0.020)/0.004) - 1)/(exp(2*(x1-0.020)/0.004) + 1))/2)
  }

  Species
  {
    name = electrons
    m = 9.11E-31
    q = -1.6e-19
    collisionModel = 1
  }
  Species
  {
    name = ions
    m = IONMASS
    q = 1.6e-19
    collisionModel = 2
  }

  Equipotential
  {
    C = 300

    Segment
    {
      j1 = 0
      j2 = 0
      k1 = 0
      k2 = KMAX
      normal = 1
    }
  }

  Equipotential
  {
    C = 0

    Segment
    {
      j1 = JMAX
      j2 = JMAX
      k1 = 0
      k2 = KMAX
      normal = -1
    }
  }


  EmitPort
//BeamEmitter
  {
    j1 = JMAX-1
    k1 = 0
    j2 = JMAX-1
    k2 = KMAX
    speciesName = electrons
    units = EV
    normal = -1
    np2c = NPIM
    thetadot = 0.0
    I = 4.5
    v1drift = 0
    v2drift = 0
    v3drift = 0
    v1thermal = 1.0
    v2thermal = 1.0
    v3thermal = 1.0
  }

  Load
  {
    speciesName = electrons
    np2c = NPIM
    density = plasmaDensityMKS

    x1MinMKS = 0.0
    x1MaxMKS = maxLength - 0.001
    x2MinMKS = 0.0
    x2MaxMKS = maxHeight

    units = EV
    v1drift = 0.0
    v2drift = 0.0
    v3drift = 0.0

    v1thermal = 1.0
    v2thermal = 1.0
    v3thermal = 0.0
  }
  Load
  {
    speciesName = ions
    np2c = NPIM
    density = plasmaDensityMKS

    x1MinMKS = 0.0
    x1MaxMKS = maxLength - 0.001
    x2MinMKS = 0.0
    x2MaxMKS = maxHeight

    units = EV
    v1drift = 0.0
    v2drift = 0.0
    v3drift = 0.0

    v1thermal = 1.0
    v2thermal = 1.0
    v3thermal = 0.0
  }

}
