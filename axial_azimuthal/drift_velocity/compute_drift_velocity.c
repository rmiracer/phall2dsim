#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LX 0.04
#define LY 0.02

#define M 257
#define N 257

double B(double z, double phi)
{
  double Bval;
  double modBmax, muxBField, sigmaBFieldsquared1, sigmaBFieldsquared2;

  modBmax = 170*1.e-4;
  muxBField = 0.025;
  sigmaBFieldsquared1 = 0.0080*0.0080;
  sigmaBFieldsquared2 = 0.0200*0.0200;

  if(muxBField - z > 0)
  {
    Bval = modBmax*exp( -(z - muxBField)*(z - muxBField)/sigmaBFieldsquared1);
  }
  else
  {
    Bval = modBmax*exp( -(z - muxBField)*(z - muxBField)/sigmaBFieldsquared2);
  }

  return Bval;
}

main()
{
  int i, j;
  double *z, *phi, *E;
  FILE *fpin, *fpout;

  if(!(fpin = fopen("./z_phi_Ezphi.dat", "r")))
  {
    fprintf(stderr, "Failed to open input file\n");
  }
  else if(!(fpout = fopen("./output.dat", "w")))
  {
    fprintf(stderr, "Failed to open output file\n");
  }
  else
  {
    z   = (double *)malloc(sizeof(double)*M*N);
    phi = (double *)malloc(sizeof(double)*M*N);
    E   = (double *)malloc(sizeof(double)*2*M*N);

    for(i = 0; i < M; i++)
    {
      for(j = 0; j < N; j++)
      {
        fscanf(fpin, "%lf %lf %lf %lf\n", &z[i*M + j], &phi[i*M + j], &E[i*M*2 + j*2 + 0], &E[i*M*2 + j*2 + 1]);
      }
    }

    // drift velocity Vd = (E x B)/B^2
    for(i = 0; i < M; i++)
    {
      for(j = 0; j < N; j++)
      {
        fprintf(fpout, "%lf %lf %lf %lf\n", z[i*M + j], phi[i*M + j],
                        E[i*M*2 + j*2 + 1]/B(z[i*M + j], phi[i*M + j]),
                       -E[i*M*2 + j*2 + 0]/B(z[i*M + j], phi[i*M + j])
               );
      }
    }

    fclose(fpin);
    fclose(fpout);

    free(z);
    free(phi);
    free(E);
    z   = NULL;
    phi = NULL;
    E   = NULL;
  }
}
