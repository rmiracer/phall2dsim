if [ -f temp.dmp ]; then
  rm temp.dmp
fi

if [ -f data_ascii.dat ]; then
  rm data_ascii.dat
fi

for i in `seq 1 200`; do

  echo $i

  ln -s ../phall_zphi99_106_50_1199_106_130_$i.dmp temp.dmp

  h5dump -d /Fields/Edl       -o tempE.dat -y temp.dmp > /dev/null
  h5dump -d /Fields/positions -o temppos.dat -y temp.dmp > /dev/null

  paste temppos.dat tempE.dat | awk '{print $1, $2, $3, $4}' | sed 's/,/ /g' > z_phi_Ezphi.dat

  ./compute_drift_velocity.exe

  cat output.dat >> data_ascii.dat
#  echo >> data_ascii.dat

  rm temp.dmp

done
