if [ ! -d frames_Ey ]; then
  mkdir frames_Ey
fi

if [ ! -d frames_phi ]; then
  mkdir frames_phi
fi

if [ ! -d frames_part ]; then
  mkdir frames_part
fi

if [ ! -d frames_elec ]; then
  mkdir frames_elec
fi

if [ ! -d frames_ions_z_Vz ]; then
  mkdir frames_ions_z_Vz
fi

if [ -f temp.dmp ]; then
  rm temp.dmp
fi

if [ -f nelec.dat ]; then
  rm nelec.dat
fi

if [ -f nions1.dat ]; then
  rm nions1.dat
fi

if [ -f t_ne.dat ]; then
  rm t_ne.dat
fi

if [ -f t_ni.dat ]; then
  rm t_ni.dat
fi

if [ -f temp.dmp ]; then
  rm temp.dmp
fi

##### Get a file list sorted by simulation time

DIR='../*.dmp'

if [ -f time_filenames.dat ]; then

  rm time_filenames.dat

fi

for i in `ls -1rt $DIR`; do

  echo $i

  j=`basename $i`;

  h5dump -a /SpatialRegion/simulationTime -y $i | head -n 6 | tail -n 1 > time.dat

  time=`cat time.dat`;

  echo $time $i >> time_filenames.dat

done

cat time_filenames.dat | sort -g | awk '{print $2}' > filenames_sort_by_time.dat

rm time_filenames.dat

####

BEGIN=1
END=`ls -1 $DIR | wc -l`;

echo $BEGIN, $END

#for i in `seq $BEGIN $END`; do
i=$BEGIN

#for file in `ls -1rt $DIR/*.dmp`; do
for file in `cat filenames_sort_by_time.dat`; do

  echo $i/$END
  echo '  ' $file

#  Skip files already plotted
#  if [ $i -gt 1195 ]; then

  ln -s $file ./temp.dmp

  h5dump -d /Fields/BdS       -o tempphi.dat -y temp.dmp > /dev/null
  h5dump -d /Fields/Edl       -o tempedl.dat -y temp.dmp > /dev/null
  h5dump -d /Fields/positions -o temppos.dat -y temp.dmp > /dev/null
  h5dump -g /electrons/ -o tempelectrons.dat     -y temp.dmp > /dev/null
  h5dump -g /ions/      -o tempions1.dat         -y temp.dmp > /dev/null

  # Annoying XML output of h5dump
  h5dump -a /SpatialRegion/simulationTime -y temp.dmp | head -n 6 | tail -n 1 > time.dat

  paste temppos.dat tempphi.dat | awk '{print $1, $2, $5}' | sed 's/,/ /g' > phi.dat
  paste temppos.dat tempedl.dat | awk '{print $1, $2, $4}' | sed 's/,/ /g' > edl.dat
  cat tempelectrons.dat         | awk '{print $1, $2}'     | sed 's/,/ /g' > electrons.dat
  cat tempions1.dat              | awk '{print $1, $2, $3, $4}'     | sed 's/,/ /g' > ions1.dat 

#  sed 's/,/ /g' phi_comma.dat > phi.dat

  gnuplot plot_phi.gplot
  mv fig.png frames_phi/frame`printf "%04d" $i`.png

  gnuplot plot_Ey.gplot
  mv fig.png frames_Ey/frame`printf "%04d" $i`.png

  gnuplot plot_phi_particles.gplot
  mv fig.png frames_part/frame`printf "%04d" $i`.png

  gnuplot plot_phi_electrons.gplot
  mv fig.png frames_elec/frame`printf "%04d" $i`.png

  gnuplot plot_ions_z_Vz.gplot
  mv fig.png frames_ions_z_Vz/frame`printf "%04d" $i`.png

  wc -l electrons.dat  > nelec.dat
  wc -l ions1.dat      > nions1.dat

  paste time.dat nelec.dat  | awk '{print $1, $2}' >> t_ne.dat
  paste time.dat nions1.dat | awk '{print $1, $2}' >> t_ni.dat


  rm temp.dmp tempphi.dat tempedl.dat temppos.dat tempelectrons.dat tempions1.dat nelec.dat nions1.dat

#  fi

  i=$(($i+1))

done
