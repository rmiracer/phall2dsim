% Compute the rate of plasma generation
% needed for the parameter 'sourceRate' of PlasmaSource in phall2drz.inp

clear all

% Time step
dt = 1.e-12;
% Current (A)
I = 4.5;
% Electron charge
qe = 1.602e-19;

Channel_Length = 0.025;
Exit_Length    = 0.01;

% Position of lower dielectric wall
R1 = 0.035;

% Position of upper dielectric wall
R2 = 0.05;

NZ = 128;

NPIM = 1e5

num_particles_per_sec = I/qe;

L = Channel_Length + Exit_Length;

dz = L/NZ;

length_region = 23*dz

% Area of annular region emitting electrons
area = pi*(R2**2 - R1**2)

% Volume
volume = area*length_region

% Plasma rate in particles per sec, per volume
% Recall that every virtual particle represents NPIM real particles!
plasmaRate = num_particles_per_sec/(NPIM*volume)
