wallZend     = 0.025;
channelRinit = 0.035;
channelRend  = 0.050;

maxZ = wallZend + 0.01;

minR = 0.025;
maxR = 0.055;

dx = maxZ/256;
dy = (maxR - minR)/256;

[x y] = meshgrid([0:dx:(maxZ-dx)], [minR:dy:(maxR-dy)]);

modB = 0.012;

muxBField = wallZend;
muyBField = channelRinit;
sigmaxBFieldsquared = 0.008*0.008;
sigmayBFieldsquared = 0.024*0.024;

B2init = (1-heaviside(y - channelRend)).*heaviside(y - channelRinit).*exp((-1/2)*(  ( (x - muxBField).*(x - muxBField)./sigmaxBFieldsquared) + (y - muyBField).*(y - muyBField)./sigmayBFieldsquared ) );

