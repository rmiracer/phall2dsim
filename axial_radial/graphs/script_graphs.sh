export LANG=
PWD=..

if [ ! -d frames_phi ]; then
  mkdir frames_phi
fi

if [ ! -d frames_ptcls ]; then
  mkdir frames_ptcls
fi

if [ ! -d frames_electrons ]; then
  mkdir frames_electrons
fi

if [ -f phall2dzr_temp.h5 ]; then
  rm phall2dzr_temp.h5 
fi

##### Get a file list sorted by simulation time

DIR='../phall2dzr*.dmp'

if [ -f time_filenames.dat ]; then

  rm time_filenames.dat

fi

for i in `ls -1rt $DIR`; do

  echo $i

  j=`basename $i`;

  h5dump -a /SpatialRegion/simulationTime -y $i | head -n 6 | tail -n 1 > time.dat

  time=`cat time.dat`;

  echo $time $i >> time_filenames.dat

done

cat time_filenames.dat | sort -g | awk '{print $2}' > filenames_sort_by_time.dat

rm time_filenames.dat

echo "  Finished sorting files by time"

i=1
#for file in `ls -1rt ../ppt2dsim_*.h5`; do
for file in `cat filenames_sort_by_time.dat`; do
  echo $file

  # Skip files already plotted
#  if [ $i -gt 403 ]; then

  ln -s $file ./temp.h5

  h5dump -d /Fields/BdS       -o tempphi.dat -y temp.h5 > /dev/null
  h5dump -d /Fields/positions -o temppos.dat -y temp.h5 > /dev/null

  h5dump -g /electrons/ -o tempelectrons.dat     -y temp.h5 > /dev/null
		    #  h5dump -g /plasmaelectrons/ -o temppelectrons.dat     -y temp.h5 > /dev/null
  h5dump -g /ixenon/      -o tempions.dat         -y temp.h5 > /dev/null
  h5dump -a /SpatialRegion/simulationTime -y temp.h5 | head -n 6 | tail -n 1 > time.dat

  paste temppos.dat tempphi.dat | awk '{print $1, $2, $5}' | sed 's/,/ /g' > phi.dat
  cat tempelectrons.dat | awk '{print $1, $2}'  | sed 's/,/ /g' > electrons.dat
#  cat temppelectrons.dat | awk '{print $1, $2}' | sed 's/,/ /g' > pelectrons.dat
  cat tempions.dat      | awk '{print $1, $2}'  | sed 's/,/ /g' > ions.dat 

  # Time series of number of electron and ion particles
  # (the first line is a blank line, so we substract 1)
  numelec=`wc -l electrons.dat | awk '{print $1 - 1}'`
  numions=`wc -l ions.dat      | awk '{print $1 - 1}'`
  time=`cat time.dat | awk '{print $1}'`

  echo $time $numelec $numions >> time_series_nparticles.dat

  gnuplot plot_phi.gplot
  gnuplot plot_ions.gplot
  gnuplot plot_electrons.gplot

  mv fig_phi.png       frames_phi/frame`printf "%04d" $i`.png
  mv fig_ions.png      frames_ptcls/frame`printf "%04d" $i`.png
  mv fig_electrons.png frames_electrons/frame`printf "%04d" $i`.png

# Compute average axial velocity
  cat tempions.dat | awk -f compute_avrgVz.awk > temp_avrgVz.dat
  paste time.dat temp_avrgVz.dat >> timeseries_avrgVz.dat


  rm temp.h5

#  fi

  i=$(($i+1))

done

