BEGIN{
  sum  = 0;
  cont = 0;
} 

{
  # Geometries for SPT-100
  if(0.034 < $1 && $1 < 0.035 && 0.025 < $2 && $2 < 0.055)
  # Geometries for PHall IIc
#  if(0.0325 < $1 && $1 < 0.0335 && 0.023 < $2 && $2 < 0.063)
  {
    sum = sum + $3;
    cont++;
  }
}

END{
  if(cont != 0)
  {
    printf "%lf %d",  sum/cont, cont
  }
  else
  {
    printf "0.0 0.0"
  }
}
