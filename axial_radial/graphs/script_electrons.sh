PWD=..

if [ ! -d imgs_electrons ]; then
  mkdir imgs_electrons
fi

if [ -f phall2dzr_temp.h5 ]; then
  rm phall2dzr_temp.h5 
fi

for i in `seq 1 9`; do
  echo $i
  ln -s $PWD/phall2dzr$i.dmp phall2dzr_temp.h5
  h5dump -g /electrons/ -o electrons_output.dat --width=0 --noindex phall2dzr_temp.h5 > /dev/null
#  h5dump -a /SpatialRegion/simulationTime -y phall2dzr_temp.h5 | head -n 6 | tail -n 1 > time.dat
  rm phall2dzr_temp.h5

  gnuplot graph_electrons.gplot
  mv fig.png imgs_electrons/fig_00000$i.png
  rm electrons_output.dat
done

for i in `seq 10 99`; do
  echo $i
  ln -s $PWD/phall2dzr$i.h5 phall2dzr_temp.h5
  h5dump -g /electrons/ -o electrons_output.dat --width=0 --noindex phall2dzr_temp.h5 > /dev/null
  rm phall2dzr_temp.h5

  gnuplot graph_electrons.gplot
  mv fig.png imgs_electrons/fig_0000$i.png
  rm electrons_output.dat
done

for i in `seq 100 200`; do
  echo $i
  ln -s $PWD/phall2dzr$i.h5 phall2dzr_temp.h5
  h5dump -g /electrons/ -o electrons_output.dat --width=0 --noindex phall2dzr_temp.h5 > /dev/null
  rm phall2dzr_temp.h5

  gnuplot graph_electrons.gplot
  mv fig.png imgs_electrons/fig_00000$i.png
  rm electrons_output.dat
done
