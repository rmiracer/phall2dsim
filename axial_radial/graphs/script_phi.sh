PWD=..

if [ ! -d imgs_phi ]; then
  mkdir imgs_phi
fi

if [ -f cylHallThruster_phi_temp.h5 ]; then
  rm cylHallThruster_phi_temp.h5 
fi

if [ -f z_axis.dat ]; then
  rm z_axis.dat
fi

if [ -f r_axis.dat ]; then
  rm r_axis.dat
fi

if [ ! -f r_axis.dat ]; then

  ln -s $PWD/cylHallThrusterT_phi_1.h5 cylHallThruster_phi_temp.h5

# Prepare files with (r, z) coordinates
  h5dump -d globalGridGlobal/r -o r_temp.dat --width=0 --noindex cylHallThruster_phi_temp.h5 > /dev/null
  h5dump -d globalGridGlobal/z -o z_temp.dat --width=0 --noindex cylHallThruster_phi_temp.h5 > /dev/null
 
  # Reformat coordinate files and remove initial blank line
  cat r_temp.dat | sed -e 's/,/\n/g' > r_temp2.dat
  cat z_temp.dat | sed -e 's/,/\n/g' > z_temp2.dat

  nr=`wc -l r_temp2.dat | awk '{print $1}'`
  nz=`wc -l z_temp2.dat | awk '{print $1}'`

  cat r_temp2.dat | tail -n $nr > r.dat
  cat z_temp2.dat | tail -n $nz > z.dat

  for i in `seq 1 $nz`; do
    cat r.dat >> r_axis.dat
    echo      >> r_axis.dat
  done

  cat z.dat | awk "{for(i = 0; i < $nr; i++) print \$1}" > z_axis.dat

  if [ -f cylHallThruster_phi_temp.h5 ]; then
    rm cylHallThruster_phi_temp.h5 
  fi

fi

for i in `seq 1 9`; do
  echo $i
  ln -s $PWD/cylHallThrusterT_phi_$i.h5 cylHallThruster_phi_temp.h5

  h5dump -d phi -o phi_output.dat --width=0 --noindex cylHallThruster_phi_temp.h5 > /dev/null
  python retrieve_time_phi.py > time.dat
  rm cylHallThruster_phi_temp.h5

  cat phi_output.dat  | sed -e 's/,//g' > phi_output2.dat

  nphi=`wc -l phi_output2.dat | awk '{print $1}'`

  cat phi_output2.dat | tail -n $nphi > phi.dat



  paste z_axis.dat r_axis.dat phi.dat > rzphi.dat

  gnuplot graph_phi.gplot
  mv fig.png imgs_phi/fig_00000$i.png
  rm phi_output.dat
done

for i in `seq 10 99`; do
  echo $i
  ln -s $PWD/cylHallThrusterT_phi_$i.h5 cylHallThruster_phi_temp.h5

  h5dump -d phi -o phi_output.dat --width=0 --noindex cylHallThruster_phi_temp.h5 > /dev/null
  python retrieve_time_phi.py > time.dat
  rm cylHallThruster_phi_temp.h5

  cat phi_output.dat  | sed -e 's/,//g' > phi_output2.dat

  nphi=`wc -l phi_output2.dat | awk '{print $1}'`

  cat phi_output2.dat | tail -n $nphi > phi.dat



  paste z_axis.dat r_axis.dat phi.dat > rzphi.dat

  gnuplot graph_phi.gplot
  mv fig.png imgs_phi/fig_0000$i.png
  rm phi_output.dat
done

for i in `seq 100 400`; do
  echo $i
  ln -s $PWD/cylHallThrusterT_phi_$i.h5 cylHallThruster_phi_temp.h5

  h5dump -d phi -o phi_output.dat --width=0 --noindex cylHallThruster_phi_temp.h5 > /dev/null
  python retrieve_time_phi.py > time.dat
  rm cylHallThruster_phi_temp.h5

  cat phi_output.dat  | sed -e 's/,//g' > phi_output2.dat

  nphi=`wc -l phi_output2.dat | awk '{print $1}'`

  cat phi_output2.dat | tail -n $nphi > phi.dat



  paste z_axis.dat r_axis.dat phi.dat > rzphi.dat

  gnuplot graph_phi.gplot
  mv fig.png imgs_phi/fig_000$i.png
  rm phi_output.dat
done

